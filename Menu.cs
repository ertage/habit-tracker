namespace habit_tracker
{
	class Menu
	{
		public void MenuPrompt()
		{
			bool check = true;
			string? habit = "";
			int dataID;
			int dataQuantity;
			Database db = new Database();
			db.ConnectDatabase();

			while(check)
			{
			Console.WriteLine("MENU");
			Console.WriteLine("Choose an Option: (n)ew entry, (s)how entries, (m)odify quantity, (d)elete entry, (q)uit");
			string? menuOption = Console.ReadLine();
			switch(menuOption) 
			{
  				case "n":
				case "N":
					Console.WriteLine("Enter Habit Name:");
					habit = Console.ReadLine();
					Console.WriteLine("Enter Quantity:");
					dataQuantity = Convert.ToInt32(Console.ReadLine());
					db.DataEntry(habit, dataQuantity);
  			  	break;
  				case "s":
				case "S":
					db.ShowData();
  				break;
				case "d":
				case "D":
					Console.WriteLine("Enter ID of entry you want to remove");
					dataID = Convert.ToInt32(Console.ReadLine());
					db.DeleteData(dataID);
				break;
				case "m":
				case "M":
					Console.WriteLine("Enter ID of quantity you want to modify");
					dataID = Convert.ToInt32(Console.ReadLine());
					Console.WriteLine("Enter new quantity");
					int newQuantity = Convert.ToInt32(Console.ReadLine());
					db.ModifyData(dataID, newQuantity);
				break;
				case "q":
				case "Q":
					check = false;
				break;
  				default:
				  Console.WriteLine("Not a Valid Input! Try again!");
  				break;
			}
			}
			db.DisconnectDatabase();
		}
	}
}
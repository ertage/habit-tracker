using System.Data.SQLite;
using System.Data;

namespace habit_tracker
{
	class Database
	{
            SQLiteConnection? SqlConnection;

        public void ConnectDatabase()
        {
            SqlConnection = new SQLiteConnection("Data Source=habit_tracker.sqlite;Version=3;");
            SqlConnection.Open();
        }

        public void DisconnectDatabase()
        {
            SqlConnection?.Close();
        }
		public void DataEntry(string? name, int quantity)
		{
            string habitTable = "CREATE TABLE IF NOT EXISTS habits (ID INTEGER PRIMARY KEY, name VARCHAR(20), quantity INT)";
            SQLiteCommand command = new SQLiteCommand(habitTable, SqlConnection);
            command.ExecuteNonQuery();
            command.CommandText = "INSERT INTO habits (name,quantity) VALUES(@param,@param1)";
            command.Parameters.Add(new SQLiteParameter("@param", name));
            command.Parameters.Add(new SQLiteParameter("@param1", quantity));
            command.ExecuteNonQuery();
		}

        public void ShowData()
        {
            string sql = "SELECT * FROM habits";
            SQLiteCommand command = new SQLiteCommand(sql, SqlConnection);
            command.CommandText = "SELECT count(*) FROM habits";
            object result = command.ExecuteScalar();
            int resultCount = Convert.ToInt32(result);
            if (resultCount > 0)
            {
                string output = "SELECT * FROM habits";
                SQLiteCommand command1 = new SQLiteCommand(output, SqlConnection);
                Console.WriteLine("test");
                SQLiteDataReader reader = command1.ExecuteReader();
                while (reader.Read() == true)
                {
                Console.WriteLine("ID: " + reader["ID"] + "\tName: " + reader["name"]);
                }
            }
            else
            {
                Console.WriteLine("There are no entries yet");
            }
        }
        public void DeleteData(int id)
        {
            string sql = "select * from habits";
            SQLiteCommand command = new SQLiteCommand(sql, SqlConnection);
            command.CommandText = "SELECT count(*) FROM habits WHERE ID = (@id)";
            command.Parameters.Add(new SQLiteParameter("@id", id));
            object result = command.ExecuteScalar();
            int resultCount = Convert.ToInt32(result);
            if (resultCount == 1)
            {
            command.CommandText = "DELETE FROM habits WHERE ID = (@id)";
            }
            else
            {
                Console.WriteLine("This ID doesn't exist, try another one");
            }

            command.ExecuteNonQuery();
        }

        public void ModifyData(int id, int newQuantity)
        {
            string sql = "select * from habits";
            SQLiteCommand command = new SQLiteCommand(sql, SqlConnection);
            command.CommandText = "SELECT count(*) FROM habits WHERE ID = (@id)";
            command.Parameters.Add(new SQLiteParameter("@id", id));
            command.Parameters.Add(new SQLiteParameter("@newQuantity", newQuantity));
            object result = command.ExecuteScalar();
            int resultCount = Convert.ToInt32(result);
            if (resultCount == 1)
            {
                command.CommandText = "UPDATE habits SET quantity = (@newQuantity) WHERE ID = (@id)";
            }
            else
            {
                Console.WriteLine("This ID doesn't exist, try another one");
            }
            command.ExecuteNonQuery();

        }
    }
}